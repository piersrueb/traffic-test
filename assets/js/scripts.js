//  rss stuff
//  Using cors-anywhere.herokuapp.com that enables cross-origin requests to anywhere
//  https://cors-anywhere.herokuapp.com

const RSS_URL = 'https://cors-anywhere.herokuapp.com/https://www.nasa.gov/rss/dyn/breaking_news.rss';
let slides = 6;  //  slides to load limit
let counter = 0;

fetch(RSS_URL)
.then(response => response.text())
.then(str => new window.DOMParser().parseFromString(str, "text/xml"))
.then(data => {
    const items = data.querySelectorAll("item");
    let markup = '';
    items.forEach(el => {
        if(counter < 6){
            let img = el.querySelector("enclosure").getAttribute('url');
            let title = el.querySelector("title").textContent;
            let text = el.querySelector("description").textContent;
            let link = el.querySelector("guid").textContent;
            markup = `<li class="splide__slide"><div class="slide-image" style="background-image: url(${img}); "></div><div class="slide-content"><h3 class="small">${title}</h3><p>${title}</p><a class="xsmall" target="_blank" href="${link}">Read more</a></div></li>`;
            document.getElementById('carousel-wrapper').innerHTML += markup;
            counter++;
        }
    });
})

//  init carousel

.finally(function() {
    new Splide( '#splide', {
        focus    : 'center',
        perPage  : 2,
        trimSpace: false,
        fixedWidth: '778px',
        arrows: false,
        gap: '40px',
        breakpoints: {
            768: {
                fixedWidth: '90%',
            },
        }
    } ).mount();
});

//  nav menu

let menuOpen = false;
const openMenu = () => {
    if(menuOpen === false){
        document.getElementById('burger').classList.add('open');
        document.getElementById('overlay').style.opacity = '1';
        document.getElementById('main-nav').classList.add('open');
        menuOpen = true;
    } else{
        document.getElementById('burger').classList.remove('open');
        document.getElementById('main-nav').classList.remove('open');
        document.getElementById('overlay').style.opacity = '0';
        menuOpen = false;
    }
}
document.getElementById('burger').addEventListener('click', openMenu);


