//  gulpfile

var gulp = require('gulp');
	sass = require('gulp-sass');
	cleanCSS = require('gulp-clean-css');
	concat = require('gulp-concat');
	minifyJS = require('gulp-minify');
	notify = require('gulp-notify');
	connect = require('gulp-connect');

gulp.task('connect', function() {
	connect.server({
		root: __dirname,
		livereload: true
	});
}); 

//  SASS

gulp.task('compile-sass', function(){
	return gulp.src('src/scss/style.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('assets/css'))
});

gulp.task('minify-css', function() {
	return gulp.src([
		'assets/css/*.css',
	])
    .pipe(cleanCSS())
  	.pipe(concat('style.min.css'))
    .pipe(gulp.dest('assets/css/min'))
	.pipe(notify('CSS compilation complete: <%= file.relative %>'))
	.pipe(connect.reload());
});

gulp.task('styles', gulp.series('compile-sass', 'minify-css'));

gulp.task('watch',function() {
	gulp.watch('src/scss/**/*.scss', gulp.series('styles'))
	gulp.watch('src/js/*.js', gulp.series('scripts'))
});

//  JS

gulp.task('compile-js', function() {
	return gulp.src([
		'src/js/scripts.js'
  	])
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('assets/js'));
});

gulp.task('minify-js', function() {
	return gulp.src([
		'assets/js/*.js',
	])
    .pipe(minifyJS({
        ext:{
            min:'.min.js'
        },
		noSource:[],
        exclude: ['tasks'],
        ignoreFiles: ['.combo.js', '-min.js']
    }))
    .pipe(gulp.dest('assets/js/min'))
	.pipe(notify({message: 'JS compilation complete: <%= file.relative %>', onLast: true}))
	.pipe(connect.reload());
});

gulp.task('scripts', gulp.series('compile-js', 'minify-js'));
gulp.task('default', gulp.series('connect', 'styles', 'scripts', 'watch'));